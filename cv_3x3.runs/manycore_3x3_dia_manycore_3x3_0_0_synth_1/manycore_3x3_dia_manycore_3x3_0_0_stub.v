// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Mon Mar 23 16:55:36 2020
// Host        : DESKTOP-LI95D4J running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ manycore_3x3_dia_manycore_3x3_0_0_stub.v
// Design      : manycore_3x3_dia_manycore_3x3_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xczu7ev-ffvc1156-2-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "manycore_3x3,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk, gen_port_in_0_0, gen_port_out_0_0, 
  gen_port_in_0_2, gen_port_out_0_2, gen_port_in_0_4, gen_port_out_0_4, gen_port_in_2_0, 
  gen_port_out_2_0, gen_port_in_2_2, gen_port_out_2_2, gen_port_in_2_4, gen_port_out_2_4, 
  gen_port_in_4_0, gen_port_out_4_0, gen_port_in_4_2, gen_port_out_4_2, gen_port_in_4_4, 
  gen_port_out_4_4)
/* synthesis syn_black_box black_box_pad_pin="clk,gen_port_in_0_0[162:0],gen_port_out_0_0[33:0],gen_port_in_0_2[162:0],gen_port_out_0_2[33:0],gen_port_in_0_4[162:0],gen_port_out_0_4[33:0],gen_port_in_2_0[162:0],gen_port_out_2_0[33:0],gen_port_in_2_2[162:0],gen_port_out_2_2[33:0],gen_port_in_2_4[162:0],gen_port_out_2_4[33:0],gen_port_in_4_0[162:0],gen_port_out_4_0[33:0],gen_port_in_4_2[162:0],gen_port_out_4_2[33:0],gen_port_in_4_4[162:0],gen_port_out_4_4[33:0]" */;
  input clk;
  input [162:0]gen_port_in_0_0;
  output [33:0]gen_port_out_0_0;
  input [162:0]gen_port_in_0_2;
  output [33:0]gen_port_out_0_2;
  input [162:0]gen_port_in_0_4;
  output [33:0]gen_port_out_0_4;
  input [162:0]gen_port_in_2_0;
  output [33:0]gen_port_out_2_0;
  input [162:0]gen_port_in_2_2;
  output [33:0]gen_port_out_2_2;
  input [162:0]gen_port_in_2_4;
  output [33:0]gen_port_out_2_4;
  input [162:0]gen_port_in_4_0;
  output [33:0]gen_port_out_4_0;
  input [162:0]gen_port_in_4_2;
  output [33:0]gen_port_out_4_2;
  input [162:0]gen_port_in_4_4;
  output [33:0]gen_port_out_4_4;
endmodule
