/*
 * adresses.h
 *
 *  Created on: 10 de fev de 2020
 *      Author: Bruno Almeida
 */

#ifndef SRC_ADDRESSES_H_
#define SRC_ADDRESSES_H_

//	0,0 Tile definitions
#define Y_INIT_0_0		0
#define X_INIT_0_0		0
#define PROC_ADDR_0_0	0xA0000000
#define X_ADDR_0_0		PROC_ADDR_0_0 + 4
#define Y_ADDR_0_0		PROC_ADDR_0_0 + 8
#define S_ADDR_0_0		PROC_ADDR_0_0 + 12
#define F_ADDR_0_0		PROC_ADDR_0_0 + 16
#define WPX_ADDR_0_0	PROC_ADDR_0_0 + 20
#define DONE_ADDR_0_0	PROC_ADDR_0_0 + 24
#define RPX_ADDR_0_0	PROC_ADDR_0_0 + 28

//	0,86 Tile definitions
#define Y_INIT_0_2		0
#define X_INIT_0_2		86
#define PROC_ADDR_0_2	0xA0001000
#define X_ADDR_0_2		PROC_ADDR_0_2 + 4
#define Y_ADDR_0_2		PROC_ADDR_0_2 + 8
#define S_ADDR_0_2		PROC_ADDR_0_2 + 12
#define F_ADDR_0_2		PROC_ADDR_0_2 + 16
#define WPX_ADDR_0_2	PROC_ADDR_0_2 + 20
#define DONE_ADDR_0_2	PROC_ADDR_0_2 + 24
#define RPX_ADDR_0_2	PROC_ADDR_0_2 + 28

//	0,172 Tile definitions
#define Y_INIT_0_4		0
#define X_INIT_0_4		172
#define PROC_ADDR_0_4	0xA0002000
#define X_ADDR_0_4		PROC_ADDR_0_4 + 4
#define Y_ADDR_0_4		PROC_ADDR_0_4 + 8
#define S_ADDR_0_4		PROC_ADDR_0_4 + 12
#define F_ADDR_0_4		PROC_ADDR_0_4 + 16
#define WPX_ADDR_0_4	PROC_ADDR_0_4 + 20
#define DONE_ADDR_0_4	PROC_ADDR_0_4 + 24
#define RPX_ADDR_0_4	PROC_ADDR_0_4 + 28

//	86,0 Tile definitions
#define Y_INIT_2_0		86
#define X_INIT_2_0		0
#define PROC_ADDR_2_0	0xA0003000
#define X_ADDR_2_0		PROC_ADDR_2_0 + 4
#define Y_ADDR_2_0		PROC_ADDR_2_0 + 8
#define S_ADDR_2_0		PROC_ADDR_2_0 + 12
#define F_ADDR_2_0		PROC_ADDR_2_0 + 16
#define WPX_ADDR_2_0	PROC_ADDR_2_0 + 20
#define DONE_ADDR_2_0	PROC_ADDR_2_0 + 24
#define RPX_ADDR_2_0	PROC_ADDR_2_0 + 28

//	86,86 Tile definitions
#define Y_INIT_2_2		86
#define X_INIT_2_2		86
#define PROC_ADDR_2_2	0xA0004000
#define X_ADDR_2_2		PROC_ADDR_2_2 + 4
#define Y_ADDR_2_2		PROC_ADDR_2_2 + 8
#define S_ADDR_2_2		PROC_ADDR_2_2 + 12
#define F_ADDR_2_2		PROC_ADDR_2_2 + 16
#define WPX_ADDR_2_2	PROC_ADDR_2_2 + 20
#define DONE_ADDR_2_2	PROC_ADDR_2_2 + 24
#define RPX_ADDR_2_2	PROC_ADDR_2_2 + 28

//	86,172 Tile definitions
#define Y_INIT_2_4		86
#define X_INIT_2_4		172
#define PROC_ADDR_2_4	0xA0005000
#define X_ADDR_2_4		PROC_ADDR_2_4 + 4
#define Y_ADDR_2_4		PROC_ADDR_2_4 + 8
#define S_ADDR_2_4		PROC_ADDR_2_4 + 12
#define F_ADDR_2_4		PROC_ADDR_2_4 + 16
#define WPX_ADDR_2_4	PROC_ADDR_2_4 + 20
#define DONE_ADDR_2_4	PROC_ADDR_2_4 + 24
#define RPX_ADDR_2_4	PROC_ADDR_2_4 + 28

//	172,0 Tile definitions
#define Y_INIT_4_0		172
#define X_INIT_4_0		0
#define PROC_ADDR_4_0	0xA0006000
#define X_ADDR_4_0		PROC_ADDR_4_0 + 4
#define Y_ADDR_4_0		PROC_ADDR_4_0 + 8
#define S_ADDR_4_0		PROC_ADDR_4_0 + 12
#define F_ADDR_4_0		PROC_ADDR_4_0 + 16
#define WPX_ADDR_4_0	PROC_ADDR_4_0 + 20
#define DONE_ADDR_4_0	PROC_ADDR_4_0 + 24
#define RPX_ADDR_4_0	PROC_ADDR_4_0 + 28

//	172,86 Tile definitions
#define Y_INIT_4_2		172
#define X_INIT_4_2		86
#define PROC_ADDR_4_2	0xA0007000
#define X_ADDR_4_2		PROC_ADDR_4_2 + 4
#define Y_ADDR_4_2		PROC_ADDR_4_2 + 8
#define S_ADDR_4_2		PROC_ADDR_4_2 + 12
#define F_ADDR_4_2		PROC_ADDR_4_2 + 16
#define WPX_ADDR_4_2	PROC_ADDR_4_2 + 20
#define DONE_ADDR_4_2	PROC_ADDR_4_2 + 24
#define RPX_ADDR_4_2	PROC_ADDR_4_2 + 28

//	172,172 Tile definitions
#define Y_INIT_4_4		172
#define X_INIT_4_4		172
#define PROC_ADDR_4_4	0xA0008000
#define X_ADDR_4_4		PROC_ADDR_4_4 + 4
#define Y_ADDR_4_4		PROC_ADDR_4_4 + 8
#define S_ADDR_4_4		PROC_ADDR_4_4 + 12
#define F_ADDR_4_4		PROC_ADDR_4_4 + 16
#define WPX_ADDR_4_4	PROC_ADDR_4_4 + 20
#define DONE_ADDR_4_4	PROC_ADDR_4_4 + 24
#define RPX_ADDR_4_4	PROC_ADDR_4_4 + 28

#endif /* SRC_ADDRESSES_H_ */
