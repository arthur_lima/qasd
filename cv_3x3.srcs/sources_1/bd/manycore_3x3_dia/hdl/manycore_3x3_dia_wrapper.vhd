--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
--Date        : Mon Mar 23 16:48:02 2020
--Host        : DESKTOP-LI95D4J running 64-bit major release  (build 9200)
--Command     : generate_target manycore_3x3_dia_wrapper.bd
--Design      : manycore_3x3_dia_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity manycore_3x3_dia_wrapper is
  port (
    reset : in STD_LOGIC
  );
end manycore_3x3_dia_wrapper;

architecture STRUCTURE of manycore_3x3_dia_wrapper is
  component manycore_3x3_dia is
  port (
    reset : in STD_LOGIC
  );
  end component manycore_3x3_dia;
begin
manycore_3x3_dia_i: component manycore_3x3_dia
     port map (
      reset => reset
    );
end STRUCTURE;
