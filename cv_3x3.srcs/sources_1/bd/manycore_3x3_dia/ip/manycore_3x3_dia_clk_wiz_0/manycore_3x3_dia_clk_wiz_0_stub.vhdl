-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Sat Mar 21 10:58:46 2020
-- Host        : leia-workstation running 64-bit Ubuntu 18.04.3 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/bsilva/Documents/UnB/Projeto_Final/computer-vision-noc/cv_noc_3x3/cv_noc_3x3.srcs/sources_1/bd/manycore_3x3_dia/ip/manycore_3x3_dia_clk_wiz_0/manycore_3x3_dia_clk_wiz_0_stub.vhdl
-- Design      : manycore_3x3_dia_clk_wiz_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xczu7ev-ffvc1156-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity manycore_3x3_dia_clk_wiz_0 is
  Port ( 
    clk_out1 : out STD_LOGIC;
    reset : in STD_LOGIC;
    locked : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );

end manycore_3x3_dia_clk_wiz_0;

architecture stub of manycore_3x3_dia_clk_wiz_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_out1,reset,locked,clk_in1";
begin
end;
