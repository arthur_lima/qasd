-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Mon Mar 23 16:56:04 2020
-- Host        : DESKTOP-LI95D4J running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/clone_cv/cv_noc_3x3/cv_noc_3x3.srcs/sources_1/bd/manycore_3x3_dia/ip/manycore_3x3_dia_manycore_3x3_0_0/manycore_3x3_dia_manycore_3x3_0_0_stub.vhdl
-- Design      : manycore_3x3_dia_manycore_3x3_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xczu7ev-ffvc1156-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity manycore_3x3_dia_manycore_3x3_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    gen_port_in_0_0 : in STD_LOGIC_VECTOR ( 162 downto 0 );
    gen_port_out_0_0 : out STD_LOGIC_VECTOR ( 33 downto 0 );
    gen_port_in_0_2 : in STD_LOGIC_VECTOR ( 162 downto 0 );
    gen_port_out_0_2 : out STD_LOGIC_VECTOR ( 33 downto 0 );
    gen_port_in_0_4 : in STD_LOGIC_VECTOR ( 162 downto 0 );
    gen_port_out_0_4 : out STD_LOGIC_VECTOR ( 33 downto 0 );
    gen_port_in_2_0 : in STD_LOGIC_VECTOR ( 162 downto 0 );
    gen_port_out_2_0 : out STD_LOGIC_VECTOR ( 33 downto 0 );
    gen_port_in_2_2 : in STD_LOGIC_VECTOR ( 162 downto 0 );
    gen_port_out_2_2 : out STD_LOGIC_VECTOR ( 33 downto 0 );
    gen_port_in_2_4 : in STD_LOGIC_VECTOR ( 162 downto 0 );
    gen_port_out_2_4 : out STD_LOGIC_VECTOR ( 33 downto 0 );
    gen_port_in_4_0 : in STD_LOGIC_VECTOR ( 162 downto 0 );
    gen_port_out_4_0 : out STD_LOGIC_VECTOR ( 33 downto 0 );
    gen_port_in_4_2 : in STD_LOGIC_VECTOR ( 162 downto 0 );
    gen_port_out_4_2 : out STD_LOGIC_VECTOR ( 33 downto 0 );
    gen_port_in_4_4 : in STD_LOGIC_VECTOR ( 162 downto 0 );
    gen_port_out_4_4 : out STD_LOGIC_VECTOR ( 33 downto 0 )
  );

end manycore_3x3_dia_manycore_3x3_0_0;

architecture stub of manycore_3x3_dia_manycore_3x3_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,gen_port_in_0_0[162:0],gen_port_out_0_0[33:0],gen_port_in_0_2[162:0],gen_port_out_0_2[33:0],gen_port_in_0_4[162:0],gen_port_out_0_4[33:0],gen_port_in_2_0[162:0],gen_port_out_2_0[33:0],gen_port_in_2_2[162:0],gen_port_out_2_2[33:0],gen_port_in_2_4[162:0],gen_port_out_2_4[33:0],gen_port_in_4_0[162:0],gen_port_out_4_0[33:0],gen_port_in_4_2[162:0],gen_port_out_4_2[33:0],gen_port_in_4_4[162:0],gen_port_out_4_4[33:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "manycore_3x3,Vivado 2019.1";
begin
end;
