----------------------------------------------------------------------------------
-- Company: Universidade de Brasília
-- Engineer: Bruno Almeida
-- 
-- Create Date: 26.07.2019 15:44:03
-- Design Name: Register Bank
-- Module Name: breg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Register Bank with 32 registers of parametrizable size.
--  
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity breg is
    generic(
            bit_width       : natural;
            isf_x           : natural;
            isf_y           : natural;
            isf_s           : natural;
            isf_f           : natural;
            isf_px          : natural;
            regfile_size    : natural
            );
    port(
        rs, rt, rd : in STD_LOGIC_VECTOR(regfile_size-1 downto 0);
        d_in : in STD_LOGIC_VECTOR(2**bit_width-1 downto 0);
        wren, clk, spx_en, i_mem_gpx_f : in STD_LOGIC;
        gpx_en : in STD_LOGIC := 'Z'; 
        x_in : in STD_LOGIC_VECTOR(isf_x-1 downto 0);
        y_in : in STD_LOGIC_VECTOR(isf_y-1 downto 0);
        f_in : in STD_LOGIC_VECTOR(isf_f-1 downto 0);
        s_in : in STD_LOGIC_VECTOR(isf_s-1 downto 0);
        idec_px : in STD_LOGIC_VECTOR(isf_px-1 downto 0);
        mem_px : in UNSIGNED(2**bit_width-1 downto 0);
        x_out : out UNSIGNED(2**bit_width-1 downto 0);
        y_out : out UNSIGNED(2**bit_width-1 downto 0);
        f_out : out UNSIGNED(2**bit_width-1 downto 0);
        s_out : out UNSIGNED(2**bit_width-1 downto 0);
        px_out : out UNSIGNED(2**bit_width-1 downto 0);
        ra, rb : out STD_LOGIC_VECTOR(2**bit_width-1 downto 0);
        o_mem_wr_en, o_mem_gpx_f : out STD_LOGIC
        );
end breg;

architecture Behavioral of breg is
    constant N : natural := 2**bit_width;
    constant pixel_mem_init : natural := 2**regfile_size-4;
    constant c_zero : STD_LOGIC_VECTOR(N-1 downto 0) := (N-1 downto 0 => '0');
    constant c_one : STD_LOGIC_VECTOR(N-1 downto 0) := (N-1 downto 1 => '0') & '1';
    type mem_array is array(pixel_mem_init downto 0)
                   of STD_LOGIC_VECTOR(N-1 downto 0);
    type pixel_array is array(3 downto 0) of STD_LOGIC_VECTOR(N-1 downto 0);
    -- Register bank instantiation
    -- --   (more info: https://stackoverflow.com/a/30299963/10995501)
    signal mem : mem_array := (0 => c_zero,1 => c_one, others => (others => 'Z'));
    signal pixel_mem : pixel_array := (others => (others => 'Z'));
begin
    -- Read process
    process(rs, rt)
    variable rs_tmp, rt_tmp : natural;
    begin
        rs_tmp := TO_INTEGER(UNSIGNED(rs));
        rt_tmp := TO_INTEGER(UNSIGNED(rt));
        if (rs_tmp < pixel_mem_init) then
            ra <= mem(rs_tmp);
        else
            ra <= pixel_mem(rs_tmp-pixel_mem_init);
        end if;
        if (rt_tmp < pixel_mem_init) then
            rb <= mem(rt_tmp);
        else
            rb <= pixel_mem(rt_tmp-pixel_mem_init);
        end if;
    end process;
    
    -- Write process
    process(clk)
        variable index : natural;
    begin
        if RISING_EDGE(clk) then
            if wren = '1' then
                index := TO_INTEGER(UNSIGNED(rd));
                -- 1st register always zero
                -- 2nd register always 0x1
                -- 4 last registers reserved to pixel operation
                if (index > 1) and (index < pixel_mem_init) then
                    mem(index) <= d_in;
                end if;
            else
                if gpx_en = '1' then
                    index := TO_INTEGER(UNSIGNED(idec_px));
                    mem(index) <= STD_LOGIC_VECTOR(mem_px);
                end if;
            end if;
        end if;
    end process;
    
    -- GPX and SPX
    -- send/receive data to/from Pixel memory
    process(x_in, y_in, s_in, f_in, i_mem_gpx_f, idec_px, spx_en, gpx_en)
        variable index_x, index_y, index_s, index_f, index_px : natural;
    begin
        if gpx_en = '1' then
            if (i_mem_gpx_f = '0') then
                o_mem_gpx_f <= '1';
            
                -- send P-type fields to Pixel memory
                index_x := TO_INTEGER(UNSIGNED(x_in));
                x_out <= unsigned(mem(index_x));
                pixel_mem(3) <= mem(index_x);
                index_y := TO_INTEGER(UNSIGNED(y_in));
                y_out <= unsigned(mem(index_y));
                pixel_mem(2) <= mem(index_y);
                index_s := TO_INTEGER(UNSIGNED(s_in));
                s_out <= unsigned(mem(index_s));
                pixel_mem(1) <= mem(index_s);
                index_f := TO_INTEGER(UNSIGNED(f_in));
                f_out <= unsigned(mem(index_f));
                pixel_mem(0) <= mem(index_f);
            end if;
        else
            if spx_en = '1' then
                -- enable to write pixel memory
                o_mem_wr_en <= '1';
                o_mem_gpx_f <= '1';
            
                -- send P-type fields to Pixel memory
                index_x := TO_INTEGER(UNSIGNED(x_in));
                x_out <= unsigned(mem(index_x));
                pixel_mem(3) <= mem(index_x);
                index_y := TO_INTEGER(UNSIGNED(y_in));
                y_out <= unsigned(mem(index_y));
                pixel_mem(2) <= mem(index_y);
                index_s := TO_INTEGER(UNSIGNED(s_in));
                s_out <= unsigned(mem(index_s));
                pixel_mem(1) <= mem(index_s);
                index_f := TO_INTEGER(UNSIGNED(f_in));
                f_out <= unsigned(mem(index_f));
                pixel_mem(0) <= mem(index_f);                
                
                index_px := TO_INTEGER(UNSIGNED(idec_px));
                -- Keeps MSB equals 0 because of Rd/Wr bit
                px_out <= unsigned(mem(index_px));
            else
                -- send P-type high impedance
                x_out <= (others => '0');
                y_out <= (others => '0');
                s_out <= (others => '0');
                f_out <= (others => '0');
                px_out <= (others => '0');

                o_mem_wr_en <= '0';
                o_mem_gpx_f <= '0';
            end if;
        end if;
    end process;
    
end Behavioral;
